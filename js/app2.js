function buscarPais() {
    const countryName = document.getElementById('countryName').value;

    if (!countryName) {
        alert('Por favor, ingrese el nombre del país.');
        return;
    }

    const apiUrl = `https://restcountries.com/v3.1/name/${countryName}`;

    fetch(apiUrl)
        .then(response => response.json())
        .then(data => mostrarInformacion(data))
        .catch(error => {
            console.error('Error al buscar país:', error);
            alert('No se pudo encontrar información para el país especificado.');
        });
}

function mostrarInformacion(data) {
    const countryInfo = document.getElementById('countryInfo');
    const capitalElement = document.getElementById('capital');
    const languageElement = document.getElementById('language');

    if (data.length > 0) {
        const country = data[0];
        const capital = country.capital[0] || 'No disponible';
        const language = country.languages ? Object.values(country.languages)[0] : 'No disponible';

        capitalElement.textContent = capital;
        languageElement.textContent = language;
    } else {
        alert('No se pudo encontrar información para el país especificado.');
        capitalElement.textContent = '';
        languageElement.textContent = '';
    }
}
